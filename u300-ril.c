/* ST-Ericsson U300 RIL
 *
 * Copyright (C) ST-Ericsson AB 2008-2009
 * Copyright 2006, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Based on reference-ril by The Android Open Source Project.
 *
 * Heavily modified for ST-Ericsson U300 modems.
 * Author: Christian Bejram <christian.bejram@stericsson.com>
 */

#include <telephony/ril.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <alloca.h>
#include <getopt.h>
#include <sys/socket.h>
#include <cutils/sockets.h>
#include <termios.h>

#include "atchannel.h"
#include "at_tok.h"
#include "misc.h"

#include "u300-ril.h"
#include "u300-ril-callhandling.h"
#include "u300-ril-config.h"
#include "u300-ril-messaging.h"
#include "u300-ril-network.h"
#include "u300-ril-pdp.h"
#include "u300-ril-services.h"
#include "u300-ril-sim.h"
#include "u300-ril-stk.h"
#include "u300-ril-oem.h"
#include "u300-ril-requestdatahandler.h"

#define LOG_TAG "RIL"
#include <utils/Log.h>

#ifdef MBM
#include "u300-ril-error.h"
#endif

#ifdef MBM
#define RIL_VERSION_STRING "MBM u300-ril 1.6"
#else
#define RIL_VERSION_STRING  "ST-Ericsson u300-ril R2B"
#endif
#define RIL_IMEISV_VERSION  2

#define MAX_AT_RESPONSE 0x1000

#define timespec_cmp(a, b, op)         \
        ((a).tv_sec == (b).tv_sec    \
        ? (a).tv_nsec op (b).tv_nsec \
        : (a).tv_sec op (b).tv_sec)

#ifdef MBM
int global_e2nap_state = -1;
int global_e2nap_cause = -1;
static void onConnectionStateChanged(const char *s);
#endif

/*** Declarations ***/
static void onRequest(int request, void *data, size_t datalen,
                      RIL_Token t);
static int onSupports(int requestCode);
static void onCancel(RIL_Token t);
static const char *getVersion(void);
static int isRadioOn();
static void signalCloseQueues(void);
extern const char *requestToString(int request);

/*** Static Variables ***/
static const RIL_RadioFunctions s_callbacks = {
    RIL_VERSION,
    onRequest,
    currentState,
    onSupports,
    onCancel,
    getVersion
};

static RIL_RadioState sState = RADIO_STATE_UNAVAILABLE;

static pthread_mutex_t s_state_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t s_screen_state_mutex = PTHREAD_MUTEX_INITIALIZER;

static int s_screenState = 1;

typedef struct RILRequest {
    int request;
    void *data;
    size_t datalen;
    RIL_Token token;
    struct RILRequest *next;
} RILRequest;

typedef struct RILEvent {
    void (*eventCallback) (void *param);
    void *param;
    struct timespec abstime;
    struct RILEvent *next;
    struct RILEvent *prev;
} RILEvent;

typedef struct RequestQueue {
    pthread_mutex_t queueMutex;
    pthread_cond_t cond;
    RILRequest *requestList;
    RILEvent *eventList;
    char enabled;
    char closed;
} RequestQueue;

static RequestQueue s_requestQueue = {
    .queueMutex = PTHREAD_MUTEX_INITIALIZER,
    .cond = PTHREAD_COND_INITIALIZER,
    .requestList = NULL,
    .eventList = NULL,
    .enabled = 1,
    .closed = 1
};

static RequestQueue s_requestQueuePrio = {
    .queueMutex = PTHREAD_MUTEX_INITIALIZER,
    .cond = PTHREAD_COND_INITIALIZER,
    .requestList = NULL,
    .eventList = NULL,
    .enabled = 0,
    .closed = 1
};

static RequestQueue *s_requestQueues[] = {
    &s_requestQueue,
    &s_requestQueuePrio
};

static int sFD;         /* file desc of AT channel */

static const struct timeval TIMEVAL_0 = { 0, 0 };

/**
 * Enqueue a RILEvent to the request queue. isPrio specifies in what queue
 * the request will end up.
 *
 * 0 = the "normal" queue, 1 = prio queue and 2 = both. If only one queue
 * is present, then the event will be inserted into that queue.
 */
void enqueueRILEvent(int isPrio, void (*callback) (void *param), 
                     void *param, const struct timeval *relativeTime)
{
    struct timeval tv;
    char done = 0;
    RequestQueue *q = NULL;

    RILEvent *e = malloc(sizeof(RILEvent));
    memset(e, 0, sizeof(RILEvent));

    e->eventCallback = callback;
    e->param = param;

    if (relativeTime == NULL) {
        relativeTime = alloca(sizeof(struct timeval));
        memset((struct timeval *) relativeTime, 0, sizeof(struct timeval));
    }
    
    gettimeofday(&tv, NULL);

    e->abstime.tv_sec = tv.tv_sec + relativeTime->tv_sec;
    e->abstime.tv_nsec = (tv.tv_usec + relativeTime->tv_usec) * 1000;

    if (e->abstime.tv_nsec > 1000000000) {
        e->abstime.tv_sec++;
        e->abstime.tv_nsec -= 1000000000;
    }

    if (!s_requestQueuePrio.enabled || 
        (isPrio == RIL_EVENT_QUEUE_NORMAL || isPrio == RIL_EVENT_QUEUE_ALL)) {
        q = &s_requestQueue;
    } else if (isPrio == RIL_EVENT_QUEUE_PRIO) {
        q = &s_requestQueuePrio;
    }

again:
    pthread_mutex_lock(&q->queueMutex);

    if (q->eventList == NULL) {
        q->eventList = e;
    } else {
        if (timespec_cmp(q->eventList->abstime, e->abstime, > )) {
            e->next = q->eventList;
            q->eventList->prev = e;
            q->eventList = e;
        } else {
            RILEvent *tmp = q->eventList;
            do {
                if (timespec_cmp(tmp->abstime, e->abstime, > )) {
                    tmp->prev->next = e;
                    e->prev = tmp->prev;
                    tmp->prev = e;
                    e->next = tmp;
                    break;
                } else if (tmp->next == NULL) {
                    tmp->next = e;
                    e->prev = tmp;
                    break;
                }
                tmp = tmp->next;
            } while (tmp);
        }
    }
    
    pthread_cond_broadcast(&q->cond);
    pthread_mutex_unlock(&q->queueMutex);

    if (s_requestQueuePrio.enabled && isPrio == RIL_EVENT_QUEUE_ALL && !done) {
        RILEvent *e2 = malloc(sizeof(RILEvent));
        memcpy(e2, e, sizeof(RILEvent));
        e = e2;
        done = 1;
        q = &s_requestQueuePrio;

        goto again;
    }

    return;
}

/** Do post-AT+CFUN=1 initialization. */
static void onRadioPowerOn()
{
    enqueueRILEvent(RIL_EVENT_QUEUE_PRIO, pollSIMState, NULL, NULL);
}

static void storeEccList(const char *list)
{
    /* Phone number conversion as per 3GPP TS 51.011, 10.3.27 */
    size_t pos;
    size_t len = strlen(list);

    for (pos = 0; pos + 6 <= len; ++pos) {
        size_t i;
        char dst[7];

        for (i = 0; i < 6; i += 2) {
            char digit1 = char2nib(list[pos + i + 1]) + '0';
            char digit2 = char2nib(list[pos + i + 0]) + '0';

            if (digit1 > '9')
                digit1 = 0;
            if (digit2 > '9')
                digit2 = 0;
            dst[i + 0] = digit1;
            dst[i + 1] = digit2;
        }
        dst[i] = 0;

        if (dst[0]) {
            /* Store the number in the property */
            /* TODO: Add numbers to ro.ril.ecclist property */
            LOGD("[DBG]: ECC phone number: %s", dst);
        }
    }
}

/** Do post- SIM ready initialization. */
static void onSIMReady()
{
    ATResponse *atresponse = NULL;
    int err = 0;

    /* Select message service */
    at_send_command("AT+CSMS=0", NULL);

   /* Configure new messages indication 
    *  mode = 2 - Buffer unsolicited result code in TA when TA-TE link is 
    *             reserved(e.g. in on.line data mode) and flush them to the 
    *             TE after reservation. Otherwise forward them directly to 
    *             the TE. 
    *  mt   = 2 - SMS-DELIVERs (except class 2 messages and messages in the 
    *             message waiting indication group (store message)) are 
    *             routed directly to TE using unsolicited result code:
    *             +CMT: [<alpha>],<length><CR><LF><pdu> (PDU mode)
    *             Class 2 messages are handled as if <mt> = 1
    *  bm   = 2 - New CBMs are routed directly to the TE using unsolicited
    *             result code:
    *             +CBM: <length><CR><LF><pdu> (PDU mode)
    *  ds   = 1 - SMS-STATUS-REPORTs are routed to the TE using unsolicited
    *             result code: +CDS: <length><CR><LF><pdu> (PDU mode)
    *  dfr  = 0 - TA buffer of unsolicited result codes defined within this
    *             command is flushed to the TE when <mode> 1...3 is entered
    *             (OK response is given before flushing the codes).
    */
    at_send_command("AT+CNMI=2,2,2,1,0", NULL);

    /* Configure preferred message storage 
     *   mem1 = SM, mem2 = SM, mem3 = SM
     */
    at_send_command("AT+CPMS=\"SM\",\"SM\",\"SM\"", NULL);

#ifndef MBM
    /* Configure ST-Ericsson current PS bearer. */
    at_send_command("AT*EPSB=1", NULL);
#endif

    /* Subscribe to network registration events. 
     *  n = 2 - Enable network registration and location information 
     *          unsolicited result code +CREG: <stat>[,<lac>,<ci>] 
     */
    err = at_send_command("AT+CREG=2", &atresponse);
    if (err < 0 || atresponse->success == 0) {
        /* Some handsets -- in tethered mode -- don't support CREG=2. */
        at_send_command("AT+CREG=1", NULL);
    }
    at_response_free(atresponse);
    atresponse = NULL;

    /* Don't subscribe to Ericsson network registration events
     *  n = 0 - Disable network registration unsolicited result codes.
     */
    at_send_command("AT*EREG=0", NULL);

    /* Subsctibe to Call Waiting Notifications.
     *  n = 1 - Enable call waiting notifications
     */
    at_send_command("AT+CCWA=1", NULL);

    /* Configure mute control.
     *  n 0 - Mute off
     */
    at_send_command("AT+CMUT=0", NULL);

    /* Subscribe to Supplementary Services Notification
     *  n = 1 - Enable the +CSSI result code presentation status.
     *          Intermediaate result codes. When enabled and a supplementary
     *          service notification is received after a mobile originated
     *          call setup.
     *  m = 1 - Enable the +CSSU result code presentation status.
     *          Unsolicited result code. When a supplementary service 
     *          notification is received during a mobile terminated call
     *          setup or during a call, or when a forward check supplementary
     *          service notification is received.
     */
    at_send_command("AT+CSSN=1,1", NULL);

    /* Subscribe to Unstuctured Supplementary Service Data (USSD) notifications.
     *  n = 1 - Enable result code presentation in the TA.
     */
    at_send_command("AT+CUSD=1", NULL);

    /* Subscribe to Packet Domain Event Reporting.
     *  mode = 1 - Discard unsolicited result codes when ME-TE link is reserved
     *             (e.g. in on-line data mode); otherwise forward them directly
     *             to the TE.
     *   bfr = 0 - MT buffer of unsolicited result codes defined within this
     *             command is cleared when <mode> 1 is entered.
     */
    at_send_command("AT+CGEREP=1,0", NULL);

    /* Configure Short Message (SMS) Format 
     *  mode = 0 - PDU mode.
     */
    at_send_command("AT+CMGF=0", NULL);

    /* Subscribe to ST-Ericsson time zone/NITZ reporting.
     *  
     */
    at_send_command("AT*ETZR=2", NULL);

    /* Subscribe to ST-Ericsson Call monitoring events.
     *  onoff = 1 - Call monitoring is on
     */
    at_send_command("AT*ECAM=1", NULL);

    /* SIM Application Toolkit Configuration
     *  n = 1 - Enable SAT unsolicited result codes
     *  stkPrfl = - SIM application toolkit profile in hexadecimal format 
     *              starting with first byte of the profile.
     *              See 3GPP TS 11.14[1] for details.
     */
    /* TODO: Investigate if we need to set profile, or if it's ignored as
             described in the AT specification. */
    at_send_command("AT*STKC=1,\"000000000000000000\"", NULL);

    /* Configure Mobile Equipment Event Reporting.
     *  mode = 3 - Forward unsolicited result codes directly to the TE;
     *             There is no inband technique used to embed result codes
     *             and data when TA is in on-line data mode.
     */
    at_send_command("AT+CMER=3,0,0,1", NULL);
}

/**
 * RIL_REQUEST_GET_IMSI
*/
static void requestGetIMSI(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    err = at_send_command_numeric("AT+CIMI", &atresponse);

    if (err < 0 || atresponse->success == 0) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    } else {
        RIL_onRequestComplete(t, RIL_E_SUCCESS,
                              atresponse->p_intermediates->line,
                              sizeof(char *));
    }
    at_response_free(atresponse);
    return;
}

/* RIL_REQUEST_DEVICE_IDENTITY
 *
 * Request the device ESN / MEID / IMEI / IMEISV.
 *
 */
static void requestDeviceIdentity(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    char* response[4];
    int err;
#ifdef MBM
    char svn[5];
#endif

    /* IMEI */ 
    err = at_send_command_numeric("AT+CGSN", &atresponse);

    if (err < 0 || atresponse->success == 0) {
        goto error;
    } else {
        asprintf(&response[0], "%s", atresponse->p_intermediates->line);
    }

    /* IMEISV */
#ifndef MBM
    asprintf(&response[1], "%02d", RIL_IMEISV_VERSION);
#else
    at_response_free(atresponse);
    atresponse = NULL;
    err = at_send_command_multiline("AT*EVERS", "SVN", &atresponse);

    if (err < 0 || atresponse->success == 0)
        goto eevinfo;
    else
        goto parse_svn;

eevinfo:
    at_response_free(atresponse);
    atresponse = NULL;
    err = at_send_command_multiline("AT*EEVINFO", "SVN", &atresponse);

    if (err < 0 || atresponse->success == 0)
        goto error;

parse_svn:
    sscanf(atresponse->p_intermediates->line, "SVN%*s %s", svn);
    asprintf(&response[1], svn);

#endif

    /* CDMA not supported */
    response[2] = NULL;
    response[3] = NULL;

    RIL_onRequestComplete(t, RIL_E_SUCCESS,
                          &response,
                          sizeof(response));

    free(response[0]);
    free(response[1]);

finally:
    at_response_free(atresponse);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/* Deprecated */
/**
 * RIL_REQUEST_GET_IMEI
 *
 * Get the device IMEI, including check digit.
*/
static void requestGetIMEI(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *atresponse = NULL;
    int err;

    err = at_send_command_numeric("AT+CGSN", &atresponse);

    if (err < 0 || atresponse->success == 0) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    } else {
        RIL_onRequestComplete(t, RIL_E_SUCCESS,
                              atresponse->p_intermediates->line,
                              sizeof(char *));
    }
    at_response_free(atresponse);
    return;
}

/* Deprecated */
/**
 * RIL_REQUEST_GET_IMEISV
 *
 * Get the device IMEISV, which should be two decimal digits.
*/
static void requestGetIMEISV(void *data, size_t datalen, RIL_Token t)
{
    char *response = NULL;

#ifndef MBM
    asprintf(&response, "%02d", RIL_IMEISV_VERSION);
#else
    ATResponse *atresponse = NULL;
    int err;
    char svn[5];

    /* IMEISV */
    atresponse = NULL;
    err = at_send_command_multiline("AT*EVERS", "SVN", &atresponse);

    if (err < 0 || atresponse->success == 0)
        goto eevinfo;
    else
        goto parse_svn;

error:
    at_response_free(atresponse);
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    return;

eevinfo:
    at_response_free(atresponse);
    atresponse = NULL;
    err = at_send_command_multiline("AT*EEVINFO", "SVN", &atresponse);

    if (err < 0 || atresponse->success == 0)
        goto error;

parse_svn:
    sscanf(atresponse->p_intermediates->line, "SVN%*s %s", svn);
    asprintf(&response, svn);

    at_response_free(atresponse);

#endif

    RIL_onRequestComplete(t, RIL_E_SUCCESS,
                          response,
                          sizeof(char *));

    if (response)
        free(response);
}

/**
 * RIL_REQUEST_RADIO_POWER
 *
 * Toggle radio on and off (for "airplane" mode).
*/
static void requestRadioPower(void *data, size_t datalen, RIL_Token t)
{
    int onOff;
    int err;
    ATResponse *atresponse = NULL;

    assert(datalen >= sizeof(int *));
    onOff = ((int *) data)[0];

    if (onOff == 0 && sState != RADIO_STATE_OFF) {
        err = at_send_command("AT+CFUN=4", &atresponse);
        if (err < 0 || atresponse->success == 0)
            goto error;
        setRadioState(RADIO_STATE_OFF);
    } else if (onOff > 0 && sState == RADIO_STATE_OFF) {
        err = at_send_command("AT+CFUN=1", &atresponse);
        if (err < 0 || atresponse->success == 0) {
            goto error;
        }
        setRadioState(RADIO_STATE_SIM_NOT_READY);
    } else {
        LOGE("Erroneous input to requestRadioPower()!");
        goto error;
    }
    
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
finally:
    at_response_free(atresponse);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

/**
 * Will LOCK THE MUTEX! MAKE SURE TO RELEASE IT!
 */
void getScreenStateLock(void)
{
    /* Just make sure we're not changing anything with regards to screen state. */
    pthread_mutex_lock(&s_screen_state_mutex);
}

int getScreenState(void)
{
    return s_screenState;
}

void releaseScreenStateLock(void)
{
    pthread_mutex_unlock(&s_screen_state_mutex);
}

static void requestScreenState(void *data, size_t datalen, RIL_Token t)
{
    int err, screenState;

    assert(datalen >= sizeof(int *));

    pthread_mutex_lock(&s_screen_state_mutex);
    screenState = s_screenState = ((int *) data)[0];

    if (screenState == 1) {
        /* Screen is on - be sure to enable all unsolicited notifications again. */
        err = at_send_command("AT+CREG=2", NULL);
        if (err < 0)
            goto error;
        err = at_send_command("AT+CGREG=2", NULL);
        if (err < 0)
            goto error;
        err = at_send_command("AT+CGEREP=1,0", NULL);
        if (err < 0)
            goto error;
#ifndef MBM
        err = at_send_command("AT*EPSB=1", NULL);
        if (err < 0)
            goto error;
#endif
    } else if (screenState == 0) {
        /* Screen is off - disable all unsolicited notifications. */
        err = at_send_command("AT+CREG=0", NULL);
        if (err < 0)
            goto error;
        err = at_send_command("AT+CGREG=0", NULL);
        if (err < 0)
            goto error;
        err = at_send_command("AT+CGEREP=0,0", NULL);
        if (err < 0)
            goto error;
#ifndef MBM
        /* FIXME If this is causing unnessecary wakeups, add again.
           err = at_send_command("AT*EPSB=0", NULL); */
#endif
        if (err < 0)
            goto error;
    } else {
        /* Not a defined value - error. */
        goto error;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

finally:
    pthread_mutex_unlock(&s_screen_state_mutex);
    return;

error:
    LOGE("ERROR: requestScreenState failed");
    if (t != (RIL_Token) 0)
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);

    goto finally;
}

/**
 * RIL_REQUEST_BASEBAND_VERSION
 *
 * Return string value indicating baseband version, eg
 * response from AT+CGMR.
*/
static void requestBasebandVersion(void *data, size_t datalen, RIL_Token t)
{
    int err;
    ATResponse *atresponse = NULL;
    char *line;

    err = at_send_command_singleline("AT+CGMR", "\0", &atresponse);

    if (err < 0 || 
        atresponse->success == 0 || 
        atresponse->p_intermediates == NULL) {
        goto error;
    }

    line = atresponse->p_intermediates->line;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, line, sizeof(char *));

finally:
    at_response_free(atresponse);
    return;

error:
    LOGE("Error in requestBasebandVersion()");
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}

static char isPrioRequest(int request)
{
    unsigned int i;
    for (i = 0; i < sizeof(prioRequests) / sizeof(int); i++)
        if (request == prioRequests[i])
            return 1;
    return 0;
}

static void processRequest(int request, void *data, size_t datalen, RIL_Token t)
{
    LOGE("processRequest: %s", requestToString(request));

    /* Ignore all requests except RIL_REQUEST_GET_SIM_STATUS
     * when RADIO_STATE_UNAVAILABLE.
     */
    if (sState == RADIO_STATE_UNAVAILABLE
        && request != RIL_REQUEST_GET_SIM_STATUS) {
        RIL_onRequestComplete(t, RIL_E_RADIO_NOT_AVAILABLE, NULL, 0);
        return;
    }

    /* Ignore all non-power requests when RADIO_STATE_OFF
     * (except RIL_REQUEST_GET_SIM_STATUS and a few more).
     */
    if ((sState == RADIO_STATE_OFF || sState == RADIO_STATE_SIM_NOT_READY)
        && !(request == RIL_REQUEST_RADIO_POWER || 
             request == RIL_REQUEST_GET_SIM_STATUS ||
             request == RIL_REQUEST_GET_IMEISV ||
             request == RIL_REQUEST_GET_IMEI ||
             request == RIL_REQUEST_BASEBAND_VERSION ||
             request == RIL_REQUEST_SCREEN_STATE)) {
        RIL_onRequestComplete(t, RIL_E_RADIO_NOT_AVAILABLE, NULL, 0);
        return;
    }
    
    /* 
     * These commands won't accept RADIO_NOT_AVAILABLE, so we just return
     * GENERIC_FAILURE if we're not in SIM_STATE_READY.
     */
    if (sState != RADIO_STATE_SIM_READY
        && (request == RIL_REQUEST_WRITE_SMS_TO_SIM ||
            request == RIL_REQUEST_DELETE_SMS_ON_SIM)) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
        return;
    }

    /* Don't allow radio operations when sim is absent or locked! */
    if (sState == RADIO_STATE_SIM_LOCKED_OR_ABSENT
        && !(request == RIL_REQUEST_ENTER_SIM_PIN ||
             request == RIL_REQUEST_ENTER_SIM_PUK ||
             request == RIL_REQUEST_ENTER_SIM_PIN2 ||
             request == RIL_REQUEST_ENTER_SIM_PUK2 ||
             request == RIL_REQUEST_ENTER_NETWORK_DEPERSONALIZATION ||
             request == RIL_REQUEST_GET_SIM_STATUS ||
             request == RIL_REQUEST_RADIO_POWER ||
             request == RIL_REQUEST_GET_IMEISV ||
             request == RIL_REQUEST_GET_IMEI ||
             request == RIL_REQUEST_BASEBAND_VERSION ||
             request == RIL_REQUEST_GET_CURRENT_CALLS)) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
        return;
    }

    switch (request) {

#ifndef MBM
        /* Basic Voice Call */
        case RIL_REQUEST_LAST_CALL_FAIL_CAUSE:
            requestLastCallFailCause(data, datalen, t);
            break;
#endif
        case RIL_REQUEST_GET_CURRENT_CALLS:
            if (sState == RADIO_STATE_SIM_LOCKED_OR_ABSENT)
                RIL_onRequestComplete(t, RIL_E_RADIO_NOT_AVAILABLE, NULL, 0);
            else
                requestGetCurrentCalls(data, datalen, t);
            break;
#ifndef MBM
        case RIL_REQUEST_DIAL:
            requestDial(data, datalen, t);
            break;
        case RIL_REQUEST_HANGUP:
            requestHangup(data, datalen, t);
            break;
        case RIL_REQUEST_ANSWER:
            requestAnswer(data, datalen, t);
            break;

        /* Advanced Voice Call */
        case RIL_REQUEST_GET_CLIR:
            requestGetCLIR(data, datalen, t);
            break;
        case RIL_REQUEST_SET_CLIR:
            requestSetCLIR(data, datalen, t);
            break;
        case RIL_REQUEST_QUERY_CALL_FORWARD_STATUS:
            requestQueryCallForwardStatus(data, datalen, t);
            break;
        case RIL_REQUEST_SET_CALL_FORWARD:
            requestSetCallForward(data, datalen, t);
            break;
        case RIL_REQUEST_QUERY_CALL_WAITING:
            requestQueryCallWaiting(data, datalen, t);
            break;
        case RIL_REQUEST_SET_CALL_WAITING:
            requestSetCallWaiting(data, datalen, t);
            break;
        case RIL_REQUEST_UDUB:
            requestUDUB(data, datalen, t);
            break;
        case RIL_REQUEST_GET_MUTE:
            requestGetMute(data, datalen, t);
            break;
        case RIL_REQUEST_SET_MUTE:
            requestSetMute(data, datalen, t);
            break;
#endif
        case RIL_REQUEST_SCREEN_STATE:
            requestScreenState(data, datalen, t);
            /* Trigger a rehash of network values, just to be sure. */
            if (((int *)data)[0] == 1)
                RIL_onUnsolicitedResponse(
                                   RIL_UNSOL_RESPONSE_NETWORK_STATE_CHANGED,
                                   NULL, 0);
            break;
#ifndef MBM
        case RIL_REQUEST_QUERY_CLIP:
            requestQueryClip(data, datalen, t);
            break;
        case RIL_REQUEST_DTMF:
            requestDTMF(data, datalen, t);
            break;
        case RIL_REQUEST_DTMF_START:
            requestDTMFStart(data, datalen, t);
            break;
        case RIL_REQUEST_DTMF_STOP:
            requestDTMFStop(data, datalen, t);
            break;

        /* Multiparty Voice Call */
        case RIL_REQUEST_HANGUP_WAITING_OR_BACKGROUND:
            requestHangupWaitingOrBackground(data, datalen, t);
            break;
        case RIL_REQUEST_HANGUP_FOREGROUND_RESUME_BACKGROUND:
            requestHangupForegroundResumeBackground(data, datalen, t);
            break;
        case RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE:
            requestSwitchWaitingOrHoldingAndActive(data, datalen, t);
            break;
        case RIL_REQUEST_CONFERENCE:
            requestConference(data, datalen, t);
            break;
        case RIL_REQUEST_SEPARATE_CONNECTION:
            requestSeparateConnection(data, datalen, t);
            break;
        case RIL_REQUEST_EXPLICIT_CALL_TRANSFER:
            requestExplicitCallTransfer(data, datalen, t);
            break;
#endif

        /* Data Call Requests */
        case RIL_REQUEST_SETUP_DATA_CALL:
            requestSetupDefaultPDP(data, datalen, t);
            break;
        case RIL_REQUEST_DEACTIVATE_DATA_CALL:
            requestDeactivateDefaultPDP(data, datalen, t);
            break;
        case RIL_REQUEST_LAST_DATA_CALL_FAIL_CAUSE:
            requestLastPDPFailCause(data, datalen, t);
            break;
        case RIL_REQUEST_DATA_CALL_LIST:
            requestPDPContextList(data, datalen, t);
            break;

        /* SMS Requests */
        case RIL_REQUEST_SEND_SMS:
            requestSendSMS(data, datalen, t);
            break;
        case RIL_REQUEST_SEND_SMS_EXPECT_MORE:
            requestSendSMSExpectMore(data, datalen, t);
            break;
        case RIL_REQUEST_WRITE_SMS_TO_SIM:
            requestWriteSmsToSim(data, datalen, t);
            break;
        case RIL_REQUEST_DELETE_SMS_ON_SIM:
            requestDeleteSmsOnSim(data, datalen, t);
            break;
        case RIL_REQUEST_GET_SMSC_ADDRESS:
            requestGetSMSCAddress(data, datalen, t);
            break;
        case RIL_REQUEST_SET_SMSC_ADDRESS:
            requestSetSMSCAddress(data, datalen, t);
            break;
        case RIL_REQUEST_SMS_ACKNOWLEDGE:
            requestSMSAcknowledge(data, datalen, t);
            break;
        case RIL_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG:
            requestGSMGetBroadcastSMSConfig(data, datalen, t);
            break;
        case RIL_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG:
            requestGSMSetBroadcastSMSConfig(data, datalen, t);
            break;
        case RIL_REQUEST_GSM_SMS_BROADCAST_ACTIVATION:
            requestGSMSMSBroadcastActivation  (data, datalen, t);
            break;

        /* SIM Handling Requests */
        case RIL_REQUEST_SIM_IO:
            requestSIM_IO(data, datalen, t);
            break;
        case RIL_REQUEST_GET_SIM_STATUS:
            requestGetSimStatus(data, datalen, t);
            break;
        case RIL_REQUEST_ENTER_SIM_PIN:
        case RIL_REQUEST_ENTER_SIM_PUK:
        case RIL_REQUEST_ENTER_SIM_PIN2:
        case RIL_REQUEST_ENTER_SIM_PUK2:
            requestEnterSimPin(data, datalen, t, request);
            break;
        case RIL_REQUEST_CHANGE_SIM_PIN:
            requestChangePassword(data, datalen, t, "SC", request);
            break;
        case RIL_REQUEST_CHANGE_SIM_PIN2:
            requestChangePassword(data, datalen, t, "P2", request);
            break;
        case RIL_REQUEST_CHANGE_BARRING_PASSWORD:
            requestChangePassword(data + sizeof(char *), 
                                  datalen - sizeof(char *), t, 
                                  ((char**) data)[0], request);
            break;
        case RIL_REQUEST_QUERY_FACILITY_LOCK:
            requestQueryFacilityLock(data, datalen, t);
            break;
        case RIL_REQUEST_SET_FACILITY_LOCK:
            requestSetFacilityLock(data, datalen, t);
            break;

        /* USSD Requests */
        case RIL_REQUEST_SEND_USSD:
            requestSendUSSD(data, datalen, t);
            break;
        case RIL_REQUEST_CANCEL_USSD:
            requestCancelUSSD(data, datalen, t);
            break;

        /* Network Selection */
        case RIL_REQUEST_SET_BAND_MODE:
            requestSetBandMode(data, datalen, t);
            break;
        case RIL_REQUEST_QUERY_AVAILABLE_BAND_MODE:
            requestQueryAvailableBandMode(data, datalen, t);
            break;
        case RIL_REQUEST_ENTER_NETWORK_DEPERSONALIZATION:
            requestEnterNetworkDepersonalization(data, datalen, t);
            break;
        case RIL_REQUEST_QUERY_NETWORK_SELECTION_MODE:
            requestQueryNetworkSelectionMode(data, datalen, t);
            break;
        case RIL_REQUEST_SET_NETWORK_SELECTION_AUTOMATIC:
            requestSetNetworkSelectionAutomatic(data, datalen, t);
            break;
        case RIL_REQUEST_SET_NETWORK_SELECTION_MANUAL:
            requestSetNetworkSelectionManual(data, datalen, t);
            break;
        case RIL_REQUEST_QUERY_AVAILABLE_NETWORKS:
            requestQueryAvailableNetworks(data, datalen, t);
            break;
        case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
            requestSetPreferredNetworkType(data, datalen, t);
            break;
        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
            requestGetPreferredNetworkType(data, datalen, t);
            break;
        case RIL_REQUEST_REGISTRATION_STATE:
        case RIL_REQUEST_GPRS_REGISTRATION_STATE:
            requestRegistrationState(request, data, datalen, t);
            break;
        case RIL_REQUEST_SET_LOCATION_UPDATES:
            requestSetLocationUpdates(data, datalen, t);
            break;

        /* OEM */
        case RIL_REQUEST_OEM_HOOK_RAW:
            requestOEMHookRaw(data, datalen, t);
            break;
        case RIL_REQUEST_OEM_HOOK_STRINGS:
            requestOEMHookStrings(data, datalen, t);
            break;

        /* Misc */
        case RIL_REQUEST_SIGNAL_STRENGTH:
            requestSignalStrength(data, datalen, t);
            break;
        case RIL_REQUEST_OPERATOR:
            requestOperator(data, datalen, t);
            break;
        case RIL_REQUEST_RADIO_POWER:
            requestRadioPower(data, datalen, t);
            break;
        case RIL_REQUEST_GET_IMSI:
            requestGetIMSI(data, datalen, t);
            break;
        case RIL_REQUEST_GET_IMEI:                  /* Deprecated */
            requestGetIMEI(data, datalen, t);
            break;
        case RIL_REQUEST_GET_IMEISV:                /* Deprecated */
            requestGetIMEISV(data, datalen, t);
            break;
        case RIL_REQUEST_DEVICE_IDENTITY:
            requestDeviceIdentity(data, datalen, t);
        case RIL_REQUEST_BASEBAND_VERSION:
            requestBasebandVersion(data, datalen, t);
            break;
        case RIL_REQUEST_SET_SUPP_SVC_NOTIFICATION:
            requestSetSuppSvcNotification(data, datalen, t);
            break;

        /* SIM Application Toolkit */
        case RIL_REQUEST_STK_SEND_TERMINAL_RESPONSE:
            requestStkSendTerminalResponse(data, datalen, t);
            break;
        case RIL_REQUEST_STK_SEND_ENVELOPE_COMMAND:
            requestStkSendEnvelopeCommand(data, datalen, t);
            break;
        case RIL_REQUEST_STK_GET_PROFILE:
            requestStkGetProfile(data, datalen, t);
            break;
        case RIL_REQUEST_STK_SET_PROFILE:
            requestStkSetProfile(data, datalen, t);
            break;
        case RIL_REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM:
            requestStkHandleCallSetupRequestedFromSIM(data, datalen, t);
            break;

        default:
            LOGW("FIXME: Unsupported request logged: %s",
                 requestToString(request));
            RIL_onRequestComplete(t, RIL_E_REQUEST_NOT_SUPPORTED, NULL, 0);
            break;
    }
}

/*** Callback methods from the RIL library to us ***/

/**
 * Call from RIL to us to make a RIL_REQUEST.
 *
 * Must be completed with a call to RIL_onRequestComplete().
 */
static void onRequest(int request, void *data, size_t datalen, RIL_Token t)
{
    RILRequest *r;
    char done = 0;
    RequestQueue *q = &s_requestQueue;

    if (s_requestQueuePrio.enabled && isPrioRequest(request))
        q = &s_requestQueuePrio;

    r = malloc(sizeof(RILRequest));  
    memset(r, 0, sizeof(RILRequest));

    /* Formulate a RILRequest and put it in the queue. */
    r->request = request;
    r->data = dupRequestData(request, data, datalen);
    r->datalen = datalen;
    r->token = t;

    pthread_mutex_lock(&q->queueMutex);

    /* Queue empty, just throw r on top. */
    if (q->requestList == NULL) {
        q->requestList = r;
    } else {
        RILRequest *l = q->requestList;
        while (l->next != NULL)
            l = l->next;

        l->next = r;
    }

    pthread_cond_broadcast(&q->cond);
    pthread_mutex_unlock(&q->queueMutex);
}

/**
 * Synchronous call from the RIL to us to return current radio state.
 * RADIO_STATE_UNAVAILABLE should be the initial state.
 */
RIL_RadioState currentState()
{
    return sState;
}

/**
 * Call from RIL to us to find out whether a specific request code
 * is supported by this implementation.
 *
 * Return 1 for "supported" and 0 for "unsupported".
 *
 * Currently just stubbed with the default value of one. This is currently
 * not used by android, and therefore not implemented here. We return
 * RIL_E_REQUEST_NOT_SUPPORTED when we encounter unsupported requests.
 */
static int onSupports(int requestCode)
{
    LOGI("onSupports() called!");

    return 1;
}

/** 
 * onCancel() is currently stubbed, because android doesn't use it and
 * our implementation will depend on how a cancellation is handled in 
 * the upper layers.
 */
static void onCancel(RIL_Token t)
{
    LOGI("onCancel() called!");
}

static const char *getVersion(void)
{
    return RIL_VERSION_STRING;
}

void setRadioState(RIL_RadioState newState)
{
    RIL_RadioState oldState;

    pthread_mutex_lock(&s_state_mutex);

    oldState = sState;

   if (sState != newState) {
        sState = newState;
    }

    pthread_mutex_unlock(&s_state_mutex);

    /* Do these outside of the mutex. */
    if (sState != oldState || sState == RADIO_STATE_SIM_LOCKED_OR_ABSENT) {
        RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_RADIO_STATE_CHANGED,
                                  NULL, 0);

        if (sState == RADIO_STATE_SIM_READY) {
            enqueueRILEvent(RIL_EVENT_QUEUE_PRIO, onSIMReady, NULL, NULL);
        } else if (sState == RADIO_STATE_SIM_NOT_READY) {
            enqueueRILEvent(RIL_EVENT_QUEUE_NORMAL, onRadioPowerOn, NULL, NULL);
        }
    }
}

/** Returns 1 if on, 0 if off, and -1 on error. */
static int isRadioOn()
{
    ATResponse *atresponse = NULL;
    int err;
    char *line;
    int ret;

    err = at_send_command_singleline("AT+CFUN?", "+CFUN:", &atresponse);
    if (err < 0 || atresponse->success == 0) {
        /* Assume radio is off. */
        goto error;
    }

    line = atresponse->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &ret);
    if (err < 0)
        goto error;

    switch (ret) {
        case 1:         /* Full functionality (switched on) */
        case 5:         /* GSM only */
        case 6:         /* WCDMA only */
            ret = 1;
            break;

        default:
            ret = 0;
    }

    at_response_free(atresponse);

    return ret;

error:
    at_response_free(atresponse);
    return -1;
}

static char initializeCommon(void)
{
    int err = 0;

    set_pending_hotswap(0);

    if (at_handshake() < 0) {
        LOG_FATAL("Handshake failed!");
        goto error;
    }

    /* Configure/set
     *   command echo (E), result code suppression (Q), DCE response format (V)
     *
     *  E0 = DCE does not echo characters during command state and online
     *       command state
     *  Q0 = DCE transmits result codes
     *  V1 = Display verbose result codes
     */
    err = at_send_command("ATE0Q0V1", NULL);
    if (err < 0)
        goto error;

   /* Set default character set. */
    err = at_send_command("AT+CSCS=\"UTF-8\"", NULL);
    if (err < 0)
        goto error;

    /* Disable automatic answer. */
    err = at_send_command("ATS0=0", NULL);
    if (err < 0)
        goto error;

    /* Enable +CME ERROR: <err> result code and use numeric <err> values. */
    err = at_send_command("AT+CMEE=1", NULL);
    if (err < 0)
        goto error;

#ifdef MBM
    err = at_send_command("AT*E2NAP=1", NULL);
    // TODO: this command may return CME error
    if (err < 0)
        goto error;

    /* Try to register for hotswap events. Don't care if it fails. */
    err = at_send_command("AT*EESIMSWAP=1", NULL);
#endif

    /* Enable Connected Line Identification Presentation. */
    err = at_send_command("AT+COLP=0", NULL);
    if (err < 0)
        goto error;

    /* Disable Service Reporting. */
    err = at_send_command("AT+CR=0", NULL);
    if (err < 0)
        goto error;

    /* Configure carrier detect signal - 1 = DCD follows the connection. */
    err = at_send_command("AT&C=1", NULL);
    if (err < 0)
        goto error;

    /* Configure DCE response to Data Termnal Ready signal - 0 = ignore. */
    err = at_send_command("AT&D=0", NULL);
    if (err < 0)
        goto error;

    /* Configure Cellular Result Codes - 0 = Disables extended format. */
    err = at_send_command("AT+CRC=0", NULL);
    if (err < 0)
        goto error;

    /* Configure Bearer Service Type and HSCSD Non-Transparent Call
     *  +CBST
     *     7 = 9600 bps V.32
     *     0 = Asynchronous connection
     *     1 = Non-transparent connection element
     *  +CHSN
     *     1 = Wanted air interface user rate is 9,6 kbits/s
     *     1 = Wanted number of receive timeslots is 1
     *     0 = Indicates that the user is not going to change <wAiur> and /or 
     *         <wRx> during the next call
     *     4 = Indicates that the accepted channel coding for the next
     *         established non-transparent HSCSD call is 9,6 kbit/s only
     */
    err = at_send_command("AT+CBST=7,0,1;+CHSN=1,1,0,4", NULL);
    if (err < 0)
        goto error;

    /* Configure Call progress Monitoring
     *    3 = BUSY result code given if called line is busy. 
     *        No NO DIALTONE result code is given.
     *        Reports line speed together with CONNECT result code.
     */
    err = at_send_command("ATX3", NULL);
    if (err < 0)
        goto error;

    return 0;
error:
    return 1;
}

/**
 * Initialize everything that can be configured while we're still in
 * AT+CFUN=0.
 */
static char initializeChannel()
{
    ATResponse *p_response = NULL;
    int err;

    LOGI("initializeChannel()");

    setRadioState(RADIO_STATE_OFF);

    /* Configure Packet Domain Network Registration Status events
     *    2 = Enable network registration and location information
     *        unsolicited result code
     */
    err = at_send_command("AT+CGREG=2", NULL);
    if (err < 0)
        goto error;

    /* Set phone functionality.
     *    4 = Disable the phone's transmit and receive RF circuits.
     */
    err = at_send_command("AT+CFUN=4", NULL);
    if (err < 0)
        goto error;

    /* Assume radio is off on error. */
    if (isRadioOn() > 0) {
        setRadioState(RADIO_STATE_SIM_NOT_READY);
    }

    return 0;

error:
    return 1;
}

/**
 * Initialize everything that can be configured while we're still in
 * AT+CFUN=0.
 */
static char initializePrioChannel()
{
    ATResponse *p_response = NULL;
    int err;

    LOGI("initializePrioChannel()");

    /* Subscribe to ST-Ericsson Pin code event.
     *   The command requests the MS to report when the PIN code has been
     *   inserted and accepted.
     *      1 = Request for report on inserted PIN code is activated (on) 
     */
    err = at_send_command("AT*EPEE=1", NULL);
    if (err < 0)
        goto error;

    /* Subscribe to ST-Ericsson SIM State Reporting.
     *   Enable SIM state reporting on the format *ESIMSR: <sim_state>
     */
#ifndef MBM
    err = at_send_command("AT*ESIMSR=1", NULL);
    if (err < 0)
        goto error;
#endif

    return 0;

error:
    return 1;
}

/**
 * Called by atchannel when an unsolicited line appears.
 * This is called on atchannel's reader thread. AT commands may
 * not be issued here.
 */
static void onUnsolicited(const char *s, const char *sms_pdu)
{
    int err;

    /* Ignore unsolicited responses until we're initialized.
       This is OK because the RIL library will poll for initial state. */
    if (sState == RADIO_STATE_UNAVAILABLE) {
        return;
    }

    if (strStartsWith(s, "*ETZV:")) {
        /* If we're in screen state, we have disabled CREG, but the ETZV
           will catch those few cases. So we send network state changed as
           well on NITZ. */
        RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_NETWORK_STATE_CHANGED,
                                  NULL, 0);

        onNetworkTimeReceived(s);
    } else if (strStartsWith(s, "*EPEV")) {
        /* Pin event, poll SIM State! */
        enqueueRILEvent(RIL_EVENT_QUEUE_PRIO, pollSIMState, NULL, NULL);
    } else if (strStartsWith(s, "*ESIMSR")) {
        onSimStateChanged(s);
    }
#ifdef MBM
    else if(strStartsWith(s, "*E2NAP:")) {
        onConnectionStateChanged(s);
    } else if (strStartsWith(s, "*EESIMSWAP:")) {
        onSimHotswap(s);
    }
#endif // End of MBM code
    else if (strStartsWith(s, "+CRING:")
               || strStartsWith(s, "RING")) {
        RIL_onUnsolicitedResponse(RIL_UNSOL_CALL_RING, NULL, 0);
    } else if (strStartsWith(s, "NO CARRIER")
               || strStartsWith(s, "+CCWA")
               || strStartsWith(s, "BUSY")) {
        RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
                                  NULL, 0);
    } else if (strStartsWith(s, "+CREG:")
#ifndef MBM
               || strStartsWith(s, "*EPSB:")
#endif
               || strStartsWith(s, "+CGREG:")) {
        RIL_onUnsolicitedResponse(RIL_UNSOL_RESPONSE_NETWORK_STATE_CHANGED,
                                  NULL, 0);
    } else if (strStartsWith(s, "+CMT:")) {
        onNewSms(sms_pdu);
    } else if (strStartsWith(s, "+CBM:")) {
        onNewBroadcastSms(sms_pdu);
    } else if (strStartsWith(s, "+CMTI:")) {
        onNewSmsOnSIM(s);
    } else if (strStartsWith(s, "+CDS:")) {
        onNewStatusReport(sms_pdu);
    } else if (strStartsWith(s, "+CGEV:")) {
        /* Really, we can ignore NW CLASS and ME CLASS events here,
           but right now we don't since extranous
           RIL_UNSOL_PDP_CONTEXT_LIST_CHANGED calls are tolerated. */
        enqueueRILEvent(RIL_EVENT_QUEUE_NORMAL, onPDPContextListChanged,
                        NULL, NULL);
    } else if (strStartsWith(s, "+CIEV: 2")) {
        unsolSignalStrength(s);
    } else if (strStartsWith(s, "+CSSI:")) {
        onSuppServiceNotification(s, 0);
    } else if (strStartsWith(s, "+CSSU:")) {
        onSuppServiceNotification(s, 1);
    } else if (strStartsWith(s, "+CUSD:")) {
        onUSSDReceived(s);
    } else if (strStartsWith(s, "*ECAV:")) {
        onECAVReceived(s);
    } else if (strStartsWith(s, "*STKEND")) {
        RIL_onUnsolicitedResponse(RIL_UNSOL_STK_SESSION_END, NULL, 0);
    } else if (strStartsWith(s, "*STKI:")) {
        onStkProactiveCommand(s);
    } else if (strStartsWith(s, "*STKN:")) {
        onStkEventNotify(s);
    }
}

static void signalCloseQueues(void)
{
    unsigned int i;
    for (i = 0; i < (sizeof(s_requestQueues) / sizeof(RequestQueue *)); i++) {
        RequestQueue *q = s_requestQueues[i];
        pthread_mutex_lock(&q->queueMutex);
        q->closed = 1;
        pthread_cond_signal(&q->cond);
        pthread_mutex_unlock(&q->queueMutex);
    }
}

/* Called on command or reader thread. */
static void onATReaderClosed()
{
    LOGI("AT channel closed\n");

    if (!get_pending_hotswap())
        setRadioState(RADIO_STATE_UNAVAILABLE);
    signalCloseQueues();
}

/* Called on command thread. */
static void onATTimeout()
{
    LOGI("AT channel timeout; restarting..\n");
    /* Last resort, throw escape on the line, close the channel
       and hope for the best. */
    at_send_escape();

    setRadioState(RADIO_STATE_UNAVAILABLE);
    signalCloseQueues();

    /* TODO We may cause a radio reset here. */
}

#ifdef MBM
static void onConnectionStateChanged(const char *s)
{
    int m_state = -1, m_cause = -1, err;
    int len;
    char msg[255];

    strcpy(msg, s);
    len = strlen(msg);
    msg[len-2] = 0; // take out \r\n

    LOGD("MBM RIL: %s", msg);

    err = at_tok_start((char **) &s);
    if (err < 0)
        return;

    err = at_tok_nextint((char **) &s, &m_state);
    if (err < 0)
        return;

    if(m_state < E2NAP_ST_DISCONNECTED || m_state > E2NAP_ST_CONNECTED) {
        m_state = -1;
        return;
    }

    err = at_tok_nextint((char **) &s, &m_cause);
    if(err < 0) {
// It might be that there is no <cause> in line
        m_cause = -1;
    } else {
        if(m_cause < E2NAP_C_SUCCESS || m_cause > E2NAP_C_MAXIMUM)
            m_cause = -1;
    }

    global_e2nap_state = m_state;
    global_e2nap_cause = m_cause;
    LOGD("MBM RIL: E2NAP state %d cause %d", m_state, m_cause);

    mbm_check_error_cause();
}
#endif

static void usage(char *s)
{
    fprintf(stderr, "usage: %s [-z] [-p <tcp port>] [-d /dev/tty_device] [-x /dev/tty_device] [-i <network interface>]\n", s);
    exit(-1);
}

struct queueArgs {
    int port;
    char * loophost;
    const char *device_path;
    char isPrio;
    char hasPrio;
};

static void *queueRunner(void *param)
{
    int fd;
    int ret;
    struct queueArgs *queueArgs = (struct queueArgs *) param;
    struct RequestQueue *q = NULL;

    LOGI("queueRunner starting!");

    for (;;) {
        fd = -1;
        while (fd < 0) {
            if (queueArgs->port > 0) {
                if (queueArgs->loophost) {
                    fd = socket_network_client(queueArgs->loophost, queueArgs->port, SOCK_STREAM);
                } else {
                    fd = socket_loopback_client(queueArgs->port, SOCK_STREAM);
                }
            } else if (queueArgs->device_path != NULL) {
                fd = open(queueArgs->device_path, O_RDWR);
#ifndef MBM
                if (fd >= 0 && !memcmp(queueArgs->device_path, "/dev/ttyS", 9)) {
#else
                if (fd >= 0 && !memcmp(queueArgs->device_path, "/dev/ttyA", 9)) {
#endif
                    /* Disable echo on serial ports. */
                    struct termios ios;
                    tcgetattr(fd, &ios);
#ifndef MBM
                    cfmakeraw(&ios);
                    cfsetospeed(&ios, B115200);
                    cfsetispeed(&ios, B115200);
                    ios.c_cflag |= CREAD | CLOCAL;
                    tcflush(fd, TCIOFLUSH);
#else
                    ios.c_lflag = 0;
#endif
                    tcsetattr(fd, TCSANOW, &ios);
                }
            }

            if (fd < 0) {
                LOGE("FAILED to open AT channel %s (%s), retrying in 10.", 
                     queueArgs->device_path, strerror(errno));
                sleep(10);
                /* Never returns. */
            }
        }

        ret = at_open(fd, onUnsolicited);

        if (ret < 0) {
            LOGE("AT error %d on at_open\n", ret);
            at_close();
            continue;
        }

        at_set_on_reader_closed(onATReaderClosed);
        at_set_on_timeout(onATTimeout);
        
        q = &s_requestQueue;

        if(initializeCommon()) {
            LOGE("FAILED to initialize channel!");
            at_close();
            continue;
        }

        if (queueArgs->isPrio == 0) {
            q->closed = 0;
            if (initializeChannel()) {
                LOGE("FAILED to initialize channel!");
                at_close();
                continue;
            }
            at_make_default_channel();
        } else {
            q = &s_requestQueuePrio;
            q->closed = 0;
            at_set_timeout_msec(1000 * 30); 
        }

        if (queueArgs->hasPrio == 0 || queueArgs->isPrio)
            if (initializePrioChannel()) {
                LOGE("FAILED to initialize channel!");
                at_close();
                continue;
            }

        LOGE("Looping the requestQueue!");
        for (;;) {
            RILRequest *r;
            RILEvent *e;
            struct timeval tv;
            struct timespec ts;

            memset(&ts, 0, sizeof(ts));

            pthread_mutex_lock(&q->queueMutex);

            if (q->closed != 0) {
                LOGW("AT Channel error, attempting to recover..");
                pthread_mutex_unlock(&q->queueMutex);
                break;
            }

            while (q->closed == 0 && q->requestList == NULL && 
                   q->eventList == NULL) {
                pthread_cond_wait(&q->cond,
                                  &q->queueMutex);

            }

            /* eventList is prioritized, smallest abstime first. */
            if (q->closed == 0 && q->requestList == NULL && q->eventList) {
                int err = 0;
                err = pthread_cond_timedwait(&q->cond, &q->queueMutex, &q->eventList->abstime);
                if (err && err != ETIMEDOUT)
                    LOGE("timedwait returned unexpected error: %s",
                         strerror(err));
            }

            if (q->closed != 0) {
                pthread_mutex_unlock(&q->queueMutex);
                continue; /* Catch the closed bit at the top of the loop. */
            }

            e = NULL;
            r = NULL;

            gettimeofday(&tv, NULL);

            /* Saves a macro, uses some stack and clock cycles.
               TODO Might want to change this. */
            ts.tv_sec = tv.tv_sec;
            ts.tv_nsec = tv.tv_usec * 1000;

            if (q->eventList != NULL &&
                timespec_cmp(q->eventList->abstime, ts, < )) {
                e = q->eventList;
                q->eventList = e->next;
            }

            if (q->requestList != NULL) {
                r = q->requestList;
                q->requestList = r->next;
            }

            pthread_mutex_unlock(&q->queueMutex);

            if (e) {
                e->eventCallback(e->param);
                free(e);
            }

            if (r) {
               processRequest(r->request, r->data, r->datalen, r->token);
               freeRequestData(r->request, r->data, r->datalen);
               free(r);
            }
        }

        at_close();
        LOGI("Re-opening after close");
    }
    return NULL;
}

pthread_t s_tid_queueRunner;
pthread_t s_tid_queueRunnerPrio;

void dummyFunction(void *args)
{
    LOGE("dummyFunction: %p", args);
}

const RIL_RadioFunctions *RIL_Init(const struct RIL_Env *env, int argc,
                                   char **argv)
{
    int ret;
    int opt;
    int port = -1;
    char *loophost = NULL;
    const char *device_path = NULL;
    const char *priodevice_path = NULL;
    struct queueArgs *queueArgs;
    struct queueArgs *prioQueueArgs;
    pthread_attr_t attr;

    s_rilenv = env;

    LOGI("Entering RIL_Init..");

    while (-1 != (opt = getopt(argc, argv, "z:i:p:d:s:x:"))) {
        switch (opt) {
            case 'z':
                loophost = optarg;
                LOGI("Using loopback host %s..", loophost);
                break;

            case 'i':
                ril_iface = optarg;
                LOGI("Using network interface %s as primary data channel.",
                     ril_iface);
                break;

            case 'p':
                port = atoi(optarg);
                if (port == 0) {
                    usage(argv[0]);
                    return NULL;
                }
                LOGI("Opening loopback port %d\n", port);
                break;

            case 'd':
                device_path = optarg;
                LOGI("Opening tty device %s\n", device_path);
                break;

            case 'x':
                priodevice_path = optarg;
                LOGI("Opening priority tty device %s\n", priodevice_path);
                break;
            default:
                usage(argv[0]);
                return NULL;
        }
    }

    if (ril_iface == NULL) {
#ifndef MBM
        LOGI("Network interface was not supplied, falling back on caif0!");
        ril_iface = strdup("caif0\0");
#else
        LOGI("Network interface was not supplied, falling back on usb0!");
        ril_iface = strdup("usb0\0");
#endif
    }

    if (port < 0 && device_path == NULL) {
        usage(argv[0]);
        return NULL;
    }

    queueArgs = malloc(sizeof(struct queueArgs));
    memset(queueArgs, 0, sizeof(struct queueArgs));

    queueArgs->device_path = device_path;
    queueArgs->port = port;
    queueArgs->loophost = loophost;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    if (priodevice_path != NULL) {
        prioQueueArgs = malloc(sizeof(struct queueArgs));
        memset(prioQueueArgs, 0, sizeof(struct queueArgs));
        prioQueueArgs->device_path = priodevice_path;
        prioQueueArgs->isPrio = 1;
        prioQueueArgs->hasPrio = 1;
        queueArgs->hasPrio = 1;

        s_requestQueuePrio.enabled = 1;

        pthread_create(&s_tid_queueRunnerPrio, &attr, queueRunner, prioQueueArgs);
    }

    pthread_create(&s_tid_queueRunner, &attr, queueRunner, queueArgs);
    
    return &s_callbacks;
}
