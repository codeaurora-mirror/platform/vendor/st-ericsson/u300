# Copyright (C) ST-Ericsson AB 2008-2009
# Copyright 2006 The Android Open Source Project
#
# Based on reference-ril
# Modified for ST-Ericsson U300 modems.
# Author: Christian Bejram <christian.bejram@stericsson.com>
#
# XXX using libutils for simulator build only...
#
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    u300-ril.c \
    u300-ril-callhandling.c \
    u300-ril-messaging.c \
    u300-ril-network.c \
    u300-ril-pdp.c \
    u300-ril-requestdatahandler.c \
    u300-ril-services.c \
    u300-ril-sim.c \
    u300-ril-stk.c \
    u300-ril-oem.c \
    atchannel.c \
    misc.c \
    fcp_parser.c \
    at_tok.c

LOCAL_SHARED_LIBRARIES := \
    libcutils libutils libril libnetutils

# For asprinf
LOCAL_CFLAGS := -D_GNU_SOURCE

LOCAL_C_INCLUDES := $(KERNEL_HEADERS) $(TOP)/hardware/ril/libril/

# Disable prelink, or add to build/core/prelink-linux-arm.map
LOCAL_PRELINK_MODULE := false

# Build shared library
LOCAL_SHARED_LIBRARIES += \
    libcutils libutils
LOCAL_LDLIBS += -lpthread
LOCAL_CFLAGS += -DRIL_SHLIB
ifeq ($(USE_MBM_RIL),true)
    LOCAL_CFLAGS += -DMBM
    LOCAL_MODULE:= libmbm-ril
else
    LOCAL_MODULE:= libu300-ril
endif
include $(BUILD_SHARED_LIBRARY)
