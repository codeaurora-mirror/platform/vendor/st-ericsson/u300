/* ST-Ericsson U300 RIL
**
** Copyright (C) ST-Ericsson AB 2008-2009
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**
** Based on reference-ril by The Android Open Source Project.
**
** Heavily modified for ST-Ericsson U300 modems.
** Author: Christian Bejram <christian.bejram@stericsson.com>
*/

#include <stdio.h>
#include "atchannel.h"
#include "at_tok.h"
#include "misc.h"
#include <telephony/ril.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <linux/route.h>
#include <cutils/properties.h>

#ifdef MBM
#include "u300-ril-error.h"
#endif

//#define LOG_NDEBUG 0
#define LOG_TAG "RIL"
#include <utils/Log.h>

#include "u300-ril.h"

#define USE_32_SUBNET_ROUTE_WORKAROUND 1

/* External interface handling functions, not having the benefit of the h-file. */
int ifc_init();
void ifc_close();
int ifc_up(char *iname);
int ifc_down(char *iname);
int ifc_set_addr(const char *name, in_addr_t addr);
int ifc_set_mask(const char *name, in_addr_t mask);
int ifc_create_default_route(const char *name, in_addr_t addr);

/* Last pdp fail cause */
static int s_lastPdpFailCause = PDP_FAIL_ERROR_UNSPECIFIED;

#ifdef MBM
static int s_data_call = 0;
#define MBM_ENAP_WAIT_TIME 17 // seconds to wait ENAP
extern int global_e2nap_state;
extern int global_e2nap_cause;
#endif // End of MBM code


void requestOrSendPDPContextList(RIL_Token *t)
{
    ATResponse *atresponse;
    RIL_Data_Call_Response *responses;
    RIL_Data_Call_Response *response;
    ATLine *cursor;
    int err;
    int n = 0;
    int i = 0;
    char *out;

    err = at_send_command_multiline("AT+CGACT?", "+CGACT:", &atresponse);
    if (err != 0 || atresponse->success == 0) {
        if (t != NULL)
            RIL_onRequestComplete(*t, RIL_E_GENERIC_FAILURE, NULL, 0);
        else
            RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                      NULL, 0);
        return;
    }

    for (cursor = atresponse->p_intermediates; cursor != NULL;
         cursor = cursor->p_next)
        n++;

    responses = alloca(n * sizeof(RIL_Data_Call_Response));

    for (i = 0; i < n; i++) {
        responses[i].cid = -1;
        responses[i].active = -1;
        responses[i].type = "";
        responses[i].apn = "";
        responses[i].address = "";
    }

    response = responses;
    for (cursor = atresponse->p_intermediates; cursor != NULL;
         cursor = cursor->p_next) {
        char *line = cursor->line;

        err = at_tok_start(&line);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &response->cid);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &response->active);
        if (err < 0)
            goto error;

        if (response->active == 1)
            response->active = 2;

        response++;
    }

    at_response_free(atresponse);

    err = at_send_command_multiline("AT+CGDCONT?", "+CGDCONT:",
                                    &atresponse);
    if (err != 0 || atresponse->success == 0) {
        if (t != NULL)
            RIL_onRequestComplete(*t, RIL_E_GENERIC_FAILURE, NULL, 0);
        else
            RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                      NULL, 0);
        return;
    }

    for (cursor = atresponse->p_intermediates; cursor != NULL;
         cursor = cursor->p_next) {
        char *line = cursor->line;
        int cid;
        char *type;
        char *apn;
        char *address;

        err = at_tok_start(&line);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &cid);
        if (err < 0)
            goto error;

        for (i = 0; i < n; i++) {
            if (responses[i].cid == cid)
                break;
        }

        if (i >= n) {
            /* Details for a context we didn't hear about in the last request. */
            continue;
        }

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].type = alloca(strlen(out) + 1);
        strcpy(responses[i].type, out);

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].apn = alloca(strlen(out) + 1);
        strcpy(responses[i].apn, out);

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].address = alloca(strlen(out) + 1);
        strcpy(responses[i].address, out);
    }

    at_response_free(atresponse);

    if (t != NULL)
        RIL_onRequestComplete(*t, RIL_E_SUCCESS, responses,
                              n * sizeof(RIL_Data_Call_Response));
    else
        RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                  responses,
                                  n * sizeof(RIL_Data_Call_Response));

    return;

error:
    if (t != NULL)
        RIL_onRequestComplete(*t, RIL_E_GENERIC_FAILURE, NULL, 0);
    else
        RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                  NULL, 0);

    at_response_free(atresponse);
}

/**
 * RIL_UNSOL_PDP_CONTEXT_LIST_CHANGED
 *
 * Indicate a PDP context state has changed, or a new context
 * has been activated or deactivated.
*
 * See also: RIL_REQUEST_PDP_CONTEXT_LIST
 */
void onPDPContextListChanged(void *param)
{
    requestOrSendPDPContextList(NULL);
}

/**
 * RIL_REQUEST_PDP_CONTEXT_LIST
 *
 * Queries the status of PDP contexts, returning for each
 * its CID, whether or not it is active, and its PDP type,
 * APN, and PDP adddress.
*/
void requestPDPContextList(void *data, size_t datalen, RIL_Token t)
{
    requestOrSendPDPContextList(&t);
}

/**
 * RIL_REQUEST_SETUP_DATA_CALL
 *
 * Configure and activate PDP context for default IP connection.
 *
 */
#ifndef MBM
void requestSetupDefaultPDP(void *data, size_t datalen, RIL_Token t)
{
    const char *APN = NULL;
    char *cmd = NULL;
    char *property = NULL;
    char *ipAddrStr = NULL;
    char *subnetMaskStr = NULL;
    char *mtuStr = NULL;
    char *defaultGatewayStr = NULL;
    char **rilResponse = NULL;
    int err;
    ATResponse *atresponse = NULL;
    ATLine *currentLine = NULL;
    in_addr_t addr, subaddr;

    /* Assign inparameters. */
    APN = ((const char **) data)[2];

    /* Define PDP Context. */
    asprintf(&cmd, "AT+CGDCONT=1,\"ip\",\"%s\",\"\",0,0", APN);
    err = at_send_command(cmd, NULL);
    free(cmd);
    if (err < 0) {
        goto error;
    }
    /* Establish a primary PDP context using Datagram link, AT*EPPSD=<state>,<channel_id>,<cid> */
    err = at_send_command_multiline("AT*EPPSD=1,1,1", "   <", &atresponse);
    if (err < 0 || atresponse->success == 0) {
        int cme_err = at_get_cme_error(atresponse);

         /*
         * Possible CME errors mapped to TS 24.008 codes:
         * 132 Service option not supported (#32)
         * 133  Requested service option not subscribed (#33)
         * 134 Service option temporarily out of order (#34)
         * 148 Unspecified GPRS error
         */
        switch (cme_err) {
        case 132:
            s_lastPdpFailCause = 32;
            break;
        case 133:
            s_lastPdpFailCause = 33;
            break;
        case 134:
            s_lastPdpFailCause = 34;
            break;
        default:
            s_lastPdpFailCause = PDP_FAIL_ERROR_UNSPECIFIED;
            break;
        }
        goto error;
    }

    {
        int buflen = 0;

        /* Loop once to calculate buffer length. */
        for (currentLine = atresponse->p_intermediates;
             currentLine != NULL; currentLine = currentLine->p_next) {

            char *line = currentLine->line;

            if (line != NULL) {
                buflen += strlen(line);
            }
        }

        if (buflen > 0) {
            int pos = 0;
            char *doc = malloc(buflen);
            char *docTail = NULL;
            char *line = NULL;
            char *value = NULL;

            /* Loop and build buffer containing all lines. */
            for (currentLine = atresponse->p_intermediates;
                 currentLine != NULL;
                 currentLine = currentLine->p_next) {

                line = currentLine->line;

                if (line != NULL) {
                    strcpy(doc + pos, line);
                    pos += strlen(line);
                }
            }

            value = getFirstElementValue(doc,
                                         "<ip_address>",
                                         "</ip_address>", NULL);
            if (value != NULL) {
                LOGE("Ip Address: %s\n", value);
                ipAddrStr = value;
                value = NULL;
            }

            value = getFirstElementValue(doc, "<subnet_mask>",
                                         "</subnet_mask>", NULL);
            if (value != NULL) {
                LOGE("Subnet Mask: %s\n", value);
                subnetMaskStr = value;
                value = NULL;
            }

            value = getFirstElementValue(doc, "<mtu>", "</mtu>", NULL);
            if (value != NULL) {
                LOGE("MTU: %s\n", value);
                mtuStr = value;
                value = NULL;
            }

            value = getFirstElementValue(doc,
                                         "<default_gateway>",
                                         "</default_gateway>",
                                         NULL);
            if (value != NULL) {
                asprintf(&property, "net.%s.gw", ril_iface);
                LOGE("GW: %s\n", value);
                defaultGatewayStr = value;

                if (property_set(property, value)) {
                    LOGE("Failed to set %s.", property);
                }
                free(property);
                value = NULL;
            }

            docTail = NULL;

            value = getFirstElementValue(doc,
                                         "<dns_server>",
                                         "</dns_server>",
                                         &docTail);
            if (value != NULL) {
                asprintf(&property, "net.%s.dns1", ril_iface);
                LOGE("1st DNS Server: %s\n", value);
                if (property_set(property, value)) {
                    LOGE("FAILED to set dns1 property!");
                }

                free(value);
                free(property);
                value = NULL;
            }

            if (docTail != NULL) {
                value = getFirstElementValue(docTail,
                                             "<dns_server>",
                                             "</dns_server>",
                                             NULL);
                if (value != NULL) {
                    asprintf(&property, "net.%s.dns2", ril_iface);
                    LOGE("2nd DNS Server: %s\n", value);
                    if (property_set(property, value)) {
                        LOGE("FAILED to set dns2 property!");
                    }

                    free(value);
                    free(property);
                    value = NULL;
                }
            }
            free(doc);
            doc = NULL;
        }
    }

    /* Setup interface and add default route using android libnetutils. */
    if (inet_pton(AF_INET, ipAddrStr, &addr) <= 0) {
        LOGE("inet_pton() failed for %s!", ipAddrStr);
        goto error;
    }

    if (ifc_init()) {
        LOGE("FAILED to set up ifc!");
        goto error;
    }

    if (ifc_down(ril_iface)) {
        LOGE("Failed to bring down %s!", ril_iface);
        goto error;
    }

    if (ifc_set_addr(ril_iface, addr)) {
        LOGE("FAILED to setup interface %s!", ril_iface);
        ifc_close();
        goto error;
    }

    if (inet_pton(AF_INET, subnetMaskStr, &subaddr) <= 0) {
        LOGE("inet_pton() failed for %s!", subnetMaskStr);
        ifc_close();
        goto error;
    }
#ifdef USE_32_SUBNET_ROUTE_WORKAROUND
    /*
     * This will fake a /31 CIDR network as defined in RFC 3021 to enable us to
     * have 'normal' routes in the routing table.
     */
    if (defaultGatewayStr == NULL && subaddr == htonl(0xFFFFFFFF)) {
        in_addr_t gw;
        struct in_addr gwaddr;
        subaddr = htonl(0xFFFFFFFE);    /* 255.255.255.254, CIDR /31. */

        gw = ntohl(addr) & 0xFFFFFF00;
        gw |= (ntohl(addr) & 0x000000FF) ^ 1;

        gwaddr.s_addr = htonl(gw);

        defaultGatewayStr = strdup(inet_ntoa(gwaddr));

        asprintf(&property, "net.%s.gw", ril_iface);
        if (property_set(property, defaultGatewayStr)) {
            LOGE("Failed to set fake %s.", property);
        }

        free(property);

        LOGI("Generated new fake /31 subnet with gw: %s", defaultGatewayStr);
    }
#endif

    /* We should have ifc_set_mtu()... */

    if (mtuStr != NULL) {
        int ifc_ctl_sock;
        int mtu = 0;

        mtu = atoi(mtuStr);

        if (mtu > 1) {
            struct ifreq ifr;
            memset(&ifr, 0, sizeof(ifr));

            strcpy(ifr.ifr_name, ril_iface);
            ifr.ifr_mtu = mtu;

            ifc_ctl_sock = socket(AF_INET, SOCK_DGRAM, 0);

            if (ifc_ctl_sock < 0) {
                LOGE("Failed to obtain ifc control socket..");
                goto error;
            }

            if (ioctl(ifc_ctl_sock, SIOCSIFMTU, &ifr)) {
                LOGE("Failed to set MTU %s!", mtuStr);
                goto error;
            }

            close(ifc_ctl_sock);
        }
    }

    if (ifc_set_mask(ril_iface, subaddr)) {
        LOGE("Failed to set subnet mask!");
        ifc_close();
        goto error;
    }

    if (ifc_up(ril_iface)) {
        LOGE("Failed to bring up %s!", ril_iface);
        ifc_close();
        goto error;
    }

    if (defaultGatewayStr != NULL) {
        in_addr_t gw;
        if (inet_pton(AF_INET, defaultGatewayStr, &gw) <= 0) {
            LOGE("Failed inet_pton for gw %s!", defaultGatewayStr);
            ifc_close();
            goto error;
        }

        if (ifc_create_default_route(ril_iface, gw)) {
            LOGE("Failed to set default route for %s!", ril_iface);
            ifc_close();
            goto error;
        }
    }
#ifndef USE_32_SUBNET_ROUTE_WORKAROUND
    /* If our netmask is 255.255.255.255, we have to set up our
       route this way. */

    /* TODO: Re-verify! */
    else if (subaddr == 0xFFFFFFFF) {
        struct rtentry rt;
        int ifc_ctl_sock;
        memset(&rt, 0, sizeof(rt));

        rt.rt_dst.sa_family = AF_INET;
        rt.rt_flags = RTF_UP;
        rt.rt_dev = ril_iface;

        ((struct sockaddr_in *) &rt.rt_genmask)->sin_family = AF_INET;

        ifc_ctl_sock = socket(AF_INET, SOCK_DGRAM, 0);

        if (ifc_ctl_sock < 0) {
            LOGE("Failed to obtain ifc control socket..");
            goto error;
        }

        if (ioctl(ifc_ctl_sock, SIOCADDRT, &rt)) {
            LOGE("Failed to set route..");
            close(ifc_ctl_sock);
            goto error;
        }

        close(ifc_ctl_sock);
    }
#endif
    else {
        LOGE("Did not receive a default gateway..");
    }

    ifc_close();

    rilResponse = alloca(3 * sizeof(char *));

    rilResponse[0] = "1";
    rilResponse[1] = ril_iface;
    rilResponse[2] = ipAddrStr;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, rilResponse, 3 * sizeof(char *));

finally:

    if (ipAddrStr != NULL)
        free(ipAddrStr);

    if (defaultGatewayStr != NULL)
        free(defaultGatewayStr);

    if (subnetMaskStr != NULL)
        free(subnetMaskStr);

    if (mtuStr != NULL)
        free(mtuStr);

    at_response_free(atresponse);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    goto finally;
}
#endif // not MBM

#ifdef MBM
void mbm_check_error_cause()
{
    if (global_e2nap_state == E2NAP_ST_CONNECTED)
        goto exit_out; // we are connected no reason for cause

    LOGD("E2NAP state %d cause %d", global_e2nap_state , global_e2nap_cause);

    if (global_e2nap_cause < E2NAP_C_SUCCESS)
        goto exit_out;

    if (global_e2nap_cause == PDP_FAIL_OPERATOR_BARRED) {
        s_lastPdpFailCause = PDP_FAIL_OPERATOR_BARRED;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_INSUFFICIENT_RESOURCES) {
        s_lastPdpFailCause = PDP_FAIL_INSUFFICIENT_RESOURCES;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_MISSING_UKNOWN_APN) {
        s_lastPdpFailCause = PDP_FAIL_MISSING_UKNOWN_APN;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_UNKNOWN_PDP_ADDRESS_TYPE) {
        s_lastPdpFailCause = PDP_FAIL_UNKNOWN_PDP_ADDRESS_TYPE;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_USER_AUTHENTICATION) {
        s_lastPdpFailCause = PDP_FAIL_USER_AUTHENTICATION;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_ACTIVATION_REJECT_GGSN) {
        s_lastPdpFailCause = PDP_FAIL_ACTIVATION_REJECT_GGSN;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_ACTIVATION_REJECT_UNSPECIFIED) {
        s_lastPdpFailCause = PDP_FAIL_ACTIVATION_REJECT_UNSPECIFIED;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_SERVICE_OPTION_NOT_SUPPORTED) {
        s_lastPdpFailCause = PDP_FAIL_SERVICE_OPTION_NOT_SUPPORTED;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_SERVICE_OPTION_NOT_SUBSCRIBED) {
        s_lastPdpFailCause = PDP_FAIL_SERVICE_OPTION_NOT_SUBSCRIBED;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_SERVICE_OPTION_OUT_OF_ORDER) {
        s_lastPdpFailCause = PDP_FAIL_SERVICE_OPTION_OUT_OF_ORDER;
        goto finally;
    }

    if (global_e2nap_cause == PDP_FAIL_NSAPI_IN_USE) {
        s_lastPdpFailCause = PDP_FAIL_NSAPI_IN_USE;
        goto finally;
    }

// Protocol errors from 95 - 111
// Standard defines only 95 - 101 and 111
// Those 102-110 are missing
    if (global_e2nap_cause >= GRPS_SEM_INCORRECT_MSG &&
        global_e2nap_cause <= GPRS_MSG_NOT_COMP_PROTO_STATE) {
        s_lastPdpFailCause = PDP_FAIL_PROTOCOL_ERRORS;
        goto finally;
    }

    if (global_e2nap_cause == GPRS_PROTO_ERROR_UNSPECIFIED) {
        s_lastPdpFailCause = PDP_FAIL_PROTOCOL_ERRORS;
        goto finally;
    }

exit_out:
    s_lastPdpFailCause = PDP_FAIL_ERROR_UNSPECIFIED;
finally:
    return;
}
#endif

#ifdef MBM
// Part of MBM code
// This function is called by libril with RIL_REQUEST_SETUP_DATA_CALL request.
void requestSetupDefaultPDP(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    in_addr_t addr = 0;
    in_addr_t subaddr = 0;
    const char *apn, *user, *pass;
    char *address = NULL;
    char *cmd = NULL;
    int err = -1;
    int fd, qmistatus, cme_err, i, n = 0;
    char *property = NULL;
    char *ipAddrStr = NULL;
    char *mtuStr = NULL;
    char *defaultGatewayStr = NULL;
    char **rilResponse = NULL;
    size_t cur = 0, len = 0;
    ssize_t written, rlen;
    char status[32] = { 0 };
    int retry = 10;
    char *line = NULL;
    char *p = NULL;
    int dnscnt = 0, enap = 0;
    char *response[3] = { "1", "usb0", "0.0.0.0" };

    apn = ((const char **) data)[2];
    user = ((const char **) data)[3];
    pass = ((const char **) data)[4];

    s_lastPdpFailCause = PDP_FAIL_ERROR_UNSPECIFIED;
    global_e2nap_state = -1; // lets reset E2NAP values
    global_e2nap_cause = -1; // lets reset E2NAP values

    LOGD("requesting data connection to APN '%s'", apn);

    if (ifc_init()) {
        LOGE("FAILED to set up ifc!");
        goto error;
    }

    if (ifc_down(ril_iface)) {
        LOGE("Failed to bring down %s!", ril_iface);
        goto error;
    }

    asprintf(&cmd, "AT+CGDCONT=1,\"IP\",\"%s\"", apn);
    err = at_send_command(cmd, &p_response);
    if (err < 0 || (p_response == NULL) || p_response->success == 0) {
        cme_err = at_get_cme_error(p_response);
        LOGE("CGDCONT failed %d  cme %d", err, cme_err);
        goto error;
    }
    at_response_free(p_response);
    p_response = NULL;
    free(cmd);
    cmd = NULL;

    asprintf(&cmd, "AT*EIAAUW=1,1,\"%s\",\"%s\"", user, pass);
    err = at_send_command(cmd, NULL);
    if (err < 0)
        goto error;
    free(cmd);
    cmd = NULL;

    // Start data on PDP context 1
    err = at_send_command("AT*ENAP=1,1", &p_response);
    if (err < 0 || (p_response == NULL) || p_response->success == 0) {
        cme_err = at_get_cme_error(p_response);
        LOGE("ENAP failed %d  cme %d", err, cme_err);
        goto error;
    }
    at_response_free(p_response);
    p_response = NULL;

    for (i = 0; i < MBM_ENAP_WAIT_TIME; i++) {
        err = at_send_command_singleline("AT*ENAP?", "*ENAP:", &p_response);
        if (err < 0 || (p_response == NULL) || (p_response->success == 0))
            goto error;

        line = p_response->p_intermediates->line;

        err = at_tok_start(&line);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &enap);
        if (err < 0)
            goto error;

        at_response_free(p_response);
        p_response = NULL;

        if (global_e2nap_state == E2NAP_ST_CONNECTED ||
            global_e2nap_state == E2NAP_ST_DISCONNECTED) {
            LOGD("MBM RIL: out with connected or disconnected");
            break;
        }

        if (enap == ENAP_T_CONNECTED) {
            LOGD("MBM RIL: out with enap connected");
            break;
        }

        sleep(1);
    }

    // If disconnection comes immediately after connection
    LOGD("MBM RIL: enap %d, estate %d, ecause %d", enap, global_e2nap_state, global_e2nap_cause);

    if (enap == ENAP_T_NOT_CONNECTED ||
        global_e2nap_state == E2NAP_ST_DISCONNECTED ||
        enap == ENAP_T_CONN_IN_PROG)
        goto error;

    s_data_call = 1;

    /* *E2IPCFG:
     *  (1,"10.155.68.129")(2,"10.155.68.131")(3,"80.251.192.244")(3,"80.251.192.245")
     */
    err = at_send_command_singleline("AT*E2IPCFG?", "*E2IPCFG:",
                                   &p_response);
    if (err < 0 || p_response->success == 0 || p_response->p_intermediates == NULL)
        goto error;

    p = p_response->p_intermediates->line;
    while (*p != '\0') {
        if (*p == '(')
            n++;
        p++;
    }

    p = p_response->p_intermediates->line;

    // Loop and collect information
    for (i = 0; i < n; i++) {
        int stat = 0;
        char *line = NULL;
        char *address = NULL;
        char *remaining = NULL;

        line = getFirstElementValue(p, "(", ")", &remaining);
        p = remaining;

        if (line == NULL) {
            LOGD("No more connection info.");
            break;
        }
        // <stat>
        err = at_tok_nextint(&line, &stat);
        if (err < 0)
            goto error;

        // <address>
        err = at_tok_nextstr(&line, &address);
        if (err < 0)
            goto error;

        if (stat == 1) {
            LOGD("Ip Address: %s\n", address);
            ipAddrStr = address;
        }

        if (stat == 2) {
            LOGD("GW: %s\n", address);
            defaultGatewayStr = address;
            asprintf(&property, "net.%s.gw", ril_iface);
            if (property_set(property, address))
                LOGE("FAILED to set net.%s.gw: property!", ril_iface);
        }

        if (stat == 3) {
            if (dnscnt == 0) {
                dnscnt++;
                asprintf(&property, "net.%s.dns1", ril_iface);
                LOGD("net.%s.dns1: %s\n", ril_iface, address);

                if (property_set(property, address))
                    LOGE("FAILED to set net.%s.dns1: property!",
                         ril_iface);

                if (property_set("net.dns1", address))
                    LOGE("FAILED to set net.dns1: property!");

                free(property);
            } else {
                asprintf(&property, "net.%s.dns2", ril_iface);
                LOGD("net.%s.dns2: %s\n", ril_iface, address);

                if (property_set(property, address))
                    LOGE("FAILED to set net.%s.dns2: property!",
                         ril_iface);

                if (property_set("net.dns2", address))
                    LOGE("FAILED to set dns2 property!");

                free(property);
            }
        }
    }

    LOGI("Setting up interface.");

    if (global_e2nap_state == E2NAP_ST_DISCONNECTED)
        goto error; // we got disconnected

    // Setup interface and add default route using android libnetutils
    if (inet_pton(AF_INET, ipAddrStr, &addr) <= 0) {
        LOGE("inet_pton() failed for %s!", ipAddrStr);
        goto error;
    }

    if (ifc_set_addr(ril_iface, addr)) {
        LOGE("FAILED to setup interface %s!", ril_iface);
        ifc_close();
        goto error;
    }

    if (ifc_up(ril_iface)) {
        LOGE("Failed to bring up %s!", ril_iface);
        ifc_close();
        goto error;
    }

    if (defaultGatewayStr != NULL) {
        in_addr_t gw;

        if (inet_pton(AF_INET, defaultGatewayStr, &gw) <= 0) {
            LOGE("Failed inet_pton for gw %s!", defaultGatewayStr);
            ifc_close();
            goto error;
        }

        if (ifc_create_default_route(ril_iface, gw)) {
            LOGE("Failed to set default route for %s!", ril_iface);
            ifc_close();
            goto error;
        }
    } else {
        LOGE("Did not receive a default gateway..");
    }

    ifc_close();

    response[1] = ril_iface;
    response[2] = ipAddrStr;
    LOGD("ip address %s  state %d", ipAddrStr, global_e2nap_state);

    if (global_e2nap_state == E2NAP_ST_DISCONNECTED)
        goto error; // we got disconnected

    RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));
    at_response_free(p_response);
    free(cmd);

    return;
error:

    mbm_check_error_cause();

    at_response_free(p_response);
    /* try to restore enap state */
    err = at_send_command("AT*ENAP=0", &p_response);

    s_data_call = 0;
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
    free(cmd);
}
#endif // MBM code ends

/**
 * RIL_REQUEST_DEACTIVATE_DEFAULT_PDP
 *
 * Deactivate PDP context created by RIL_REQUEST_SETUP_DEFAULT_PDP.
 *
 * See also: RIL_REQUEST_SETUP_DEFAULT_PDP.
 */
#ifndef MBM
void requestDeactivateDefaultPDP(void *data, size_t datalen, RIL_Token t)
{
    const char *pPdpCid = NULL;
    char *pCmd = NULL;
    int err;

    pPdpCid = ((const char **) data)[0];
    /* Disconnect a PDP context, AT*EPPSD=<state>,<channel_id>,<cid>  state=0 to disconnect */
    asprintf(&pCmd, "AT*EPPSD=0,1,%s", pPdpCid); /* TODO: Investigate channel_id usage. */
    err = at_send_command(pCmd, NULL);
    free(pCmd);
    if (err < 0) {
        goto error;
    }

    /* Bring down the interface as well. */
    if (ifc_init()) {
       goto error;
    }

    if (ifc_down(ril_iface)) {
        goto error;
    }

    ifc_close();

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}
#endif

#ifdef MBM
// CHECK There are several error cases if PDP deactivation fails
// 24.008: 8, 25, 36, 38, 39, 112
void requestDeactivateDefaultPDP(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int enap = 0;
    int err, i;
    char *line;

    err = at_send_command_singleline("AT*ENAP?", "*ENAP:", &p_response);
    if (err < 0 || p_response->success == 0)
        goto error;

    line = p_response->p_intermediates->line;
    err = at_tok_start(&line);
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &enap);
    if (err < 0)
        goto error;

    if (enap == ENAP_T_CONN_IN_PROG)
        LOGE("MBM RIL: when deactivating PDP, enap is IN_PROGRESS");

    if (enap == ENAP_T_CONNECTED) {
        err = at_send_command("AT*ENAP=0", NULL); // TODO can return CME error

        if (err < 0)
            goto error;
        for (i = 0; i < MBM_ENAP_WAIT_TIME; i++) {
            err = at_send_command_singleline("AT*ENAP?", "*ENAP:", &p_response);

            if (err < 0 || p_response->success == 0)
                goto error;

            line = p_response->p_intermediates->line;

            err = at_tok_start(&line);
            if (err < 0)
                goto error;

            err = at_tok_nextint(&line, &enap);
            if (err < 0)
                goto error;

            if (enap == 0)
                break;

            sleep(1);
        }

        if (enap != ENAP_T_NOT_CONNECTED)
            goto error;

        s_data_call = 0;

        /* Bring down the interface as well. */
        if (ifc_init())
            goto error;

        if (ifc_down(ril_iface))
            goto error;

        ifc_close();
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    at_response_free(p_response);
    return;

error:
    s_data_call = 0;
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}
#endif // ifdef MBM

/**
 * RIL_REQUEST_LAST_PDP_FAIL_CAUSE
 *
 * Requests the failure cause code for the most recently failed PDP
 * context activate.
 *
 * See also: RIL_REQUEST_LAST_CALL_FAIL_CAUSE.
 *
 */
void requestLastPDPFailCause(void *data, size_t datalen, RIL_Token t)
{
    RIL_onRequestComplete(t, RIL_E_SUCCESS, &s_lastPdpFailCause,
                          sizeof(int));
}
